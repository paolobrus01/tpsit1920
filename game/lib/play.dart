import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'dart:math';
import 'dart:async';

class PlayPage extends StatefulWidget {
  PlayPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PlayPageState createState() => _PlayPageState();
}

class _PlayPageState extends State<PlayPage> with TickerProviderStateMixin {
  List<List<int>> field = [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12],
    [13, 14, 15, 0]
  ];
  List<AnimationController> _controller = [];
  List<Animation<Offset>> _offset = [];

  int _secondi = 0;
  final Duration _time = Duration(milliseconds: 1000);

  StreamController<int> _streamController = StreamController.broadcast();
  StreamSubscription _cronometro;
  Stream<int> stream;
  bool _transizione = false;

  bool _isRunning = false;

  String _toTime(int time) {
    return (time ~/ 60).toString().padLeft(2, '0') +
        ':' + //MINUTES
        (time % 60).toString().padLeft(2, '0');
  }

  //INIZIALIZZAZIONE STREAMCONTROLLER E SUBSCRIBE ALLO STREAM PERIODICO
  void _play() {
    if (_cronometro == null)
      _streamController.addStream(Stream<int>.periodic(_time));
    _cronometro = _streamController.stream.listen((data) {
      _secondi++;
      setState(() {});
    });
    setState(() {
      _isRunning = true;
    });
  }

  //STOPPA MOMENTANEAMENTE IL CRONOMETRO CANCELLANDO LA SOTTOSCRIZIONE ALLO STREAM DELL'OGGETTO CRONOMETRO
  void _stop() {
    _cronometro.cancel();
    setState(() {
      _isRunning = false;
    });
  }

  //STOPPA E AZZERA IL CRONOMETRO INSIEME AGLI EVENTUALI RECORDS
  void _restartTime() {
    if (_isRunning) _stop();
    setState(() {
      _secondi = 0;
    });
  }

  //AGGIUNTA DI UN RECORD ALLA LISTA E AGGIORNAMENTO UI

  @override
  void initState() {
    super.initState();
    _restart();
    for (int i = 0; i < 16; i++) {
      _controller.add(AnimationController(
          vsync: this, duration: Duration(milliseconds: 300)));
      _offset.add(Tween<Offset>(begin: Offset.zero, end: Offset(1.0, 0.0))
          .animate(_controller[i]));
    }
    _restartTime();
    _play();
  }

  bool _controllaSchema() {
     int _temp = 0;
    for (int k = 0; k < 4; k++) {
      for (int f = 0; f < 4; f++) {
        for (int i = k; i < 4; i++) {
          for (int j = f; j < 4; j++) {
            if(field[k][f]>field[i][j]&&field[i][j]!=0) _temp++;
          }
        }
      }
    }
    return _temp % 2 == 0;
  }

  _restart() {
    Random rng = new Random();
    int counter;
    List<int> _myList;

    do {
      _myList = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
      counter = 16;

      for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
          field[i][j] = _myList.removeAt(rng.nextInt(counter));
          counter--;
        }
      }
    } while (!_controllaSchema());
  }

  void _spostamento(int index, int i, int j, double a, double b, int val1,
      int val2, int val3) {
        setState(() {
          _transizione = true;
        });
    _offset[index] = Tween<Offset>(begin: Offset.zero, end: Offset(a, b))
        .animate(_controller[index]);

    _offset[index + val3] =
        Tween<Offset>(begin: Offset.zero, end: Offset(-a, -b))
            .animate(_controller[index + val3]);

    _controller[index + val3].forward();
    _controller[index].forward().whenComplete(() {
      _controller[index].reset();
      _controller[index + val3].reset();
      setState(() {
        field[i + val1][j + val2] = field[i][j];
        field[i][j] = 0;
      });
      setState(() {
          _transizione = false;
        });
    });
    setState(() {});
  }

  _press(int index) {
    int i = index ~/ 4;
    int j = index % 4;
    double d = 1.05;

    //SINISTRA
    if (j != 0 && field[i][j - 1] == 0) {
      _spostamento(index, i, j, -d, 0.0, 0, -1, -1);
    } else
    //DESTRA
    if (j != 3 && field[i][j + 1] == 0) {
      _spostamento(index, i, j, d, 0.0, 0, 1, 1);
    } else
    //SOPRA
    if (i != 0 && field[i - 1][j] == 0) {
      _spostamento(index, i, j, 0.0, -d, -1, 0, -4);
    } else
    //SOTTO
    if (i != 3 && field[i + 1][j] == 0) {
      _spostamento(index, i, j, 0.0, d, 1, 0, 4);
    }
  }

  bool _check() {
    if (listEquals(field[0], [1, 2, 3, 4]) &&
        listEquals(field[1], [5, 6, 7, 8]) &&
        listEquals(field[2], [9, 10, 11, 12]) &&
        listEquals(field[3], [13, 14, 15, 0])) {
      _stop();
      return true;
    } else {
      return false;
    }

    ;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              _toTime(_secondi),
              style: TextStyle(fontSize: 30),
            ),
            Flexible(
                flex: 9,
                child: AbsorbPointer(
                  absorbing: _transizione,
                  child: GridView.count(
                    padding: new EdgeInsets.all(40.0),
                    crossAxisCount: 4,
                    children: List.generate(16, (idx) {
                      return Container(
                        padding: EdgeInsets.all(5.0),
                        decoration: BoxDecoration(
                            //shape: BoxShape.rectangle,
                            //border: Border.all(color: Colors.black),
                            ),
                        child: SlideTransition(
                          position: _offset[idx],
                          child: Container(
                            child: FloatingActionButton.extended(
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                              label: Text(
                                '${field[idx ~/ 4][idx % 4] != 0 ? field[idx ~/ 4][idx % 4] : ''}',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 30),
                              ),
                              backgroundColor: field[idx ~/ 4][idx % 4] == 0
                                  ? Colors.grey
                                  : Colors.blue,
                              onPressed: () {
                                _press(idx);
                              },
                              heroTag: "btn$idx",
                            ),
                          ),
                        ),
                      );
                    }),
                  ),
                )),
            Flexible(
                flex: 3,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 20,
                        height: 0,
                      ),
                      Text(
                        _check() ? 'HAI VINTO!' : '',
                        style: TextStyle(
                            fontSize: 50, fontWeight: FontWeight.bold),
                      ),
                      Container(
                        width: 20,
                        height: 0,
                      ),
                      FloatingActionButton(
                        child: Icon(Icons.autorenew),
                        onPressed: () {
                          setState(() {
                            _restart();
                            _restartTime();
                            _play();
                          });
                        },
                        heroTag: 'restartGame',
                      ),
                      Container(
                        width: 20,
                        height: 0,
                      ),
                      FloatingActionButton(
                        child: Icon(
                          Icons.motorcycle,
                        ),
                        backgroundColor: Colors.red,
                        onPressed: () {
                          setState(() {
                            field = [
                              [1, 2, 3, 4],
                              [5, 6, 7, 8],
                              [9, 10, 11, 12],
                              [13, 14, 0, 15]
                            ];
                          });
                        },
                        heroTag: 'winnableGame',
                      ),
                      Container(
                        width: 20,
                        height: 0,
                      ),
                    ]))
          ],
        ),
      ),
    );
  }
}
