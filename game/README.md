# Game

A new Flutter project.

## Applicazione

L'applicazione e' semplice e consiste in un campo di gioco del 'Gioco del 15' consistente in 16 caselle da ordinare.
A livello di codice e' una semplice matrice di bottoni che vengono scambiati tra loro fino al conseguimento della soluzione finale.
Per creare il campo da gioco i numeri vengono inseriti casualmente nella matrice, dopo che una funzione ha controllato che il gioco sia effettivamente completabile.
Ho aggiunto un cronometro riciclandolo da un vecchio progetto che comincia quando inizia una partita e si ferma alla vittoria dell'utente.
Le animazioni presenti nel codice consistono nello scorrimento delle caselle che, dopo il contatto, tramite il widget SlideTransition.
Nel gioco c'e' anche un minimo utilizzo delle routes, in quanto sono presenti due pagine consecutive che possono anche essere ampliate. 

