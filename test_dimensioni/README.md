# Test Dimensioni

***Progetto prodotto da Paolo Bruschini e Alberto Marzola***

Consegna con scadenza a fine marzo, in cui bisogna esercitarsi con il drag and drop.  

Spiagazione dei vari file

## -engine.dart

E' il cuore della applicazione, attraverso un design pattern BLOC, in cui possiamo identificare 4 stati differenti:

* **ready**: E' lo stato in cui si trova l'applicazione appena accesa e a cui ritorna una volta finito il test
* **doing**: E' lo stato in cui si trova l'applicazione durante lo svolgimento del test
* **correct**: E' lo stato in cui si trova l'applicazione una volta che il test viene superato
* **failed**: E' lo stato in cui si trova l'applicazione una volta che il test viene fallito

Lo stato *correct* e lo stato *failed* sono pressochè identici, cambia solo il messaggio visualizzato una volta completato il test.  

Questi stati vengono modificati per mezzo degli eventi:

* **start**: E' l'evento che porta lo stato da *ready* a *doing*, viene richiamato quando viene premuta la freccia iniziale del test
* **addSquare**: Questo evento una volta che viene richiamato 3 volte attraverso un controllo eseguito all'interno della funzione *_mapAddSquareToState*.  
Per essere attivato deve essere spostato un quadrato all'interno di uno dei vari dragTarget
* **restart**: Una volta terminato il test premento il pulsante restart verrà inviato questo evento che porta dallo stato di *failed* o *correct* allo stato di *ready*

## -widgets.dart

Si occupa di costruire la schermata che noi vediamo quando viene cambiato lo stato.

## -draggable.dart

Si occupa della costruzione dei tre quadrati che andranno piazzati dentro i dragTarget. Le misure di quest'ultimi vengono  
generate da un algoritmo e viene verificato che i quadrati non vengano costruiti già in ordine crescente.  
Per fare in modo che scompaiano una volta posizionati, ogni quadrato viene assegnato a una variabile e quando verrà completato  
lo spostamento alle variabile verrà assegnato un container trasparente delle stesse misure del quadrato spostato, in modo che  
si dia l'impressione che ci sia uno spazio vuoto dove prima c'era la figura.

## -draggableElement.dart

Sono degli oggetti draggable che vengono designati come dragTarget, la cui grafica viene definita all'interno di *myPainter.dart*  

## -myPainter.dart

Vieni definito un canvas e al suo interno viene disegnato un trapezio, e quando un quadrato viene trascinato all'interno del  
dragTarget viene disegnato un quadrato della grandezza del quadrato trascinato al suo interno.  

## -timer.dart

Un semplice timer che lavora in modo asincrono che parte quando lo stato passa da *ready* a *doing* e che si stoppa quando lo stato  passa da *doing* a *failed* o *correct*.
