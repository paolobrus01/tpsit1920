import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:test_dimensioni/draggableElement.dart';
import 'package:test_dimensioni/engine.dart';
import 'myPainter.dart';
import 'timer.dart';

Widget draggableWidget(DragDropBloc dragDropBloc) {
  Random _myRandom = Random();
  double dimA, dimB, dimC;

  CustomPaint _painter_a = CustomPaint(
      painter: MyPainter(
    heigthA: 50.0,
    heigthB: 90, /*data:candidateData*/
  ));
  CustomPaint _painter_b = CustomPaint(
      painter: MyPainter(
    heigthA: 90.0,
    heigthB: 130, /*data:candidateData*/
  ));
  CustomPaint _painter_c = CustomPaint(
      painter: MyPainter(
    heigthA: 130.0,
    heigthB: 170, /*data:candidateData*/
  ));

  bool acceptA = true, acceptB = true, acceptC = true;

  Timer _timer = Timer();

  //INIZIALIZZAZIONE DIMENSIONE QUADRATI
  do{
    dimA = ((_myRandom.nextInt(6) + 4) * 10).toDouble();
    do {
      dimB = ((_myRandom.nextInt(6) + 4) * 10).toDouble();
    } while (dimB == dimA);
    do {
      dimC = ((_myRandom.nextInt(6) + 4) * 10).toDouble();
    } while (dimC == dimA || dimC == dimB);
  } while((dimA < dimB) && (dimB < dimC));

  Map<double, Widget> _children = {
    dimA: MyDraggable(dimensione: dimA),
    dimB: MyDraggable(dimensione: dimB),
    dimC: MyDraggable(dimensione: dimC)
  };
  return Column(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      _timer,
      Row(children: <Widget>[
        Padding(padding: const EdgeInsets.all(5.0), child: _children[dimA]),
        Padding(padding: const EdgeInsets.all(5.0), child: _children[dimB]),
        Padding(padding: const EdgeInsets.all(5.0), child: _children[dimC]),
      ]),
      Container(height: 30),
      Row(
        children: <Widget>[
          DragTarget<double>(
            builder: (context, candidateData, rejectedData) {
              return Center(
                  child: Container(
                width: 110,
                height: 170,
                child: _painter_a,
              ));
            },
            onWillAccept: (data) {
              return acceptA;
            },
            onAccept: (data) {
              _children[data] = Container(
                height: data,
                width: data,
              );
              _painter_a = CustomPaint(
                  painter: MyPainter(heigthA: 50.0, heigthB: 90, data: data));

              dragDropBloc.add(Event(
                  event: UserEvent.addSquare,
                  measure: data,
                  position: 0,
                  timer: _timer.time));
              acceptA = false;
            },
          ),
          Container(
            width: 10,
          ),
          DragTarget<double>(
            builder: (context, candidateData, rejectedData) {
              return Center(
                  child: Container(width: 110, height: 170, child: _painter_b));
            },
            onWillAccept: (data) {
              return acceptB;
            },
            onAccept: (data) {
              _painter_b = CustomPaint(
                  painter: MyPainter(heigthA: 90.0, heigthB: 130, data: data));
              _children[data] = Container(
                height: data,
                width: data,
              );
              dragDropBloc.add(Event(
                  event: UserEvent.addSquare,
                  measure: data.toDouble(),
                  position: 1,
                  timer: _timer.time));
              acceptB = false;
            },
          ),
          Container(
            width: 10,
          ),
          DragTarget<double>(
            builder: (context, candidateData, rejectedData) {
              return Center(
                  child: Container(width: 110, height: 170, child: _painter_c));
            },
            onWillAccept: (data) {
              return acceptC;
            },
            onAccept: (data) {
              _painter_c = CustomPaint(
                  painter: MyPainter(heigthA: 130.0, heigthB: 170, data: data));
              _children[data] = Container(
                height: data,
                width: data,
              );
              dragDropBloc.add(Event(
                  event: UserEvent.addSquare,
                  measure: data.toDouble(),
                  position: 2,
                  timer: _timer.time));
              acceptC = false;
            },
          ),
        ],
      )
    ],
  );
}
