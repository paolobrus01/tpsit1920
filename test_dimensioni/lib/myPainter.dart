import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_dimensioni/engine.dart';

class MyPainter extends CustomPainter{

  MyPainter({@required this.heigthA, @required this.heigthB, this.data});
  double heigthA;
  double heigthB;
  double data;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
    ..color = Colors.blue;
    Path path = Path()
    ..moveTo(0, size.height)
    ..lineTo(0, size.height-heigthA)
    ..lineTo(size.width, size.height-heigthB)
    ..lineTo(size.width, size.height)
    ..moveTo(0, size.height);

    canvas.drawPath(path, paint);
    

    

    if(data!=null){

      path = Path()
    ..moveTo((size.width/2)-(data/2), size.height)
    ..lineTo((size.width/2)+(data/2), size.height)
    ..lineTo((size.width/2)+(data/2), size.height-data)
    ..lineTo((size.width/2)-(data/2), size.height-data);
      canvas.drawPath(path, Paint()..color=Colors.yellow);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return false;
  }


}