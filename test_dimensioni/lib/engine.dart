import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:bloc/bloc.dart';
import 'dart:async';

class DragDropBloc extends Bloc<Event, Stato> {
  //StreamSubscription<int> _tickerSubscription;

  DragDropBloc();

  @override
  Stato get initialState => Stato(state: UserState.ready, measures: [0.0, 0.0, 0.0], timer: 0);

  @override
  Stream<Stato> mapEventToState(Event e) async* {
    switch (e.event) {
      case UserEvent.start:
        yield* _mapStartToState();
        break;

      case UserEvent.addSquare:
        yield* _mapAddSquareToState(e);
        break;

      case UserEvent.restart:
        yield* _mapRestartToState();
        break;

      default:
        break;
    }
  }


  Stream<Stato> _mapStartToState() async* {
    yield Stato(state: UserState.doing, measures: state.measures, timer: state.timer);
    //To do AVVIA CRONOMETRO
  }

  Stream<Stato> _mapAddSquareToState(Event e) async* {
    
    int squares = 0;
    state.measures[e.position] = e.measure;
    for (int i = 0; i < 3; i++) {
      if (state.measures[i] != 0.0) squares++;
    }
    if (squares == 3) {
      if ((state.measures[0] < state.measures[1]) &&
          (state.measures[1] < state.measures[2]))
        yield Stato(state: UserState.correct, measures: state.measures, timer: e.timer);
      else
        yield Stato(state: UserState.failed, measures: state.measures, timer: e.timer);
    } else
      yield Stato(state: UserState.doing, measures: state.measures, timer: e.timer);
  }

  Stream<Stato> _mapRestartToState() async* {
    yield Stato(state: UserState.ready, measures: [0.0,0.0,0.0], timer: state.timer);
    //To do AVVIA CRONOMETRO
  }
}

// STATSES

enum UserState { ready, doing, correct, failed }

class Stato {
  Stato({@required this.state, @required this.measures, @required this.timer});
  UserState state;
  int timer = 0;

  List<double> measures = [null, null, null];

  @override
  String toString() => "$state";
}

// EVENTS

enum UserEvent { start, addSquare, restart }

class Event {
  UserEvent event;
  Event({@required this.event, this.measure = 0, this.position, this.timer=0});

  int position;
  int timer;
  double measure;

  @override
  String toString() => "$event";
}
