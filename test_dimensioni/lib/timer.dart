import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Timer extends StatefulWidget {
  Timer() {
    time = 0;
  }
  @override
  _TimerState createState() => _TimerState();
  int time;
  Stream<int> stream;
}

class _TimerState extends State<Timer> {
  bool active = true;

  Stream<int> _stream;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _stream = Stream.periodic(Duration(milliseconds: 100), (x) {
      setState(() {
        widget.time = x;
      });
      return x++;
    });
    _stream.listen((x) {});
  }

  String _secondsToString(int time) {
    return '${(time / 10).floor()}.${(time % 10).floor()}';
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 0.0),
        child: Text(
          _secondsToString(widget.time),
          style: TextStyle(fontSize: 35),
        ));
  }
}
