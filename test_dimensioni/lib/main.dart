import 'package:flutter/material.dart';
import 'engine.dart';
import 'widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';



void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider(
        create: (context) => DragDropBloc(),
        child: Body(title: 'test_dimensioni'),
      )
    );
  }
}
