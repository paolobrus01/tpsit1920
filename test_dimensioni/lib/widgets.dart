import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_dimensioni/engine.dart';
import 'draggable.dart';

class Body extends StatelessWidget {

  final String title;

  Body({this.title});

  static const TextStyle bodyTextStyle = TextStyle(
    fontSize: 60,
    fontWeight: FontWeight.bold,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              BlocBuilder<DragDropBloc, Stato>(
                condition: (previousState, currentState) =>
                  currentState.state != previousState.state,
                builder: (context, state) => Actions(),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class Actions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: _mapStateToActionButtons(
        dragDropBloc: BlocProvider.of<DragDropBloc>(context),
      ),
    );
  }

  String _secondsToString(int time){
    return '${(time / 10).floor()}.${(time % 10).floor()}';
  }

  List<Widget> _mapStateToActionButtons({
    DragDropBloc dragDropBloc,
  }) {
    final Stato currentState = dragDropBloc.state;
    if (currentState.state == UserState.ready) {
      return [
        FloatingActionButton(
          child: Icon(Icons.play_arrow),
          onPressed: () =>
              dragDropBloc.add(Event(event: UserEvent.start)),
        ),
      ];
    }
    if (currentState.state == UserState.doing) {
      return [
        draggableWidget(dragDropBloc),
      ];
    }
    if (currentState.state == UserState.failed) {
      
      return [
        Text('Hai FALLITO in:'),
        Text(_secondsToString(dragDropBloc.state.timer), style: TextStyle(fontSize:30),),
        FloatingActionButton(
          child: Icon(Icons.replay),
          onPressed: () => dragDropBloc.add(Event(event: UserEvent.restart)),
        ),
      ];
    }
    if (currentState.state == UserState.correct) {
      return [
        Text('Hai COMPLETATO il puzzle in:'),
        Text(_secondsToString(dragDropBloc.state.timer), style: TextStyle(fontSize:30),),
        FloatingActionButton(
          child: Icon(Icons.replay),
          onPressed: () => dragDropBloc.add(Event(event: UserEvent.restart)),
        ),
      ];
    }
    return [];
  }
}