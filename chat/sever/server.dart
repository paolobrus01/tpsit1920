import 'dart:io';
import 'dart:convert';

int _port = 8080;

Map< WebSocket,String > usersFromSocket = {};
Map< String, WebSocket > usersFromName = {};
List<String> rooms = ["DaBigRoom","Second Room"];
void main() {
  HttpServer.bind(InternetAddress.anyIPv4, _port).then((HttpServer server) {
    print("HttpServer listening...");
    server.serverHeader = "an echo server";
    server.listen((HttpRequest request) {
      // Checks whether the request is a valid WebSocket upgrade request
      if (WebSocketTransformer.isUpgradeRequest(request)){
        WebSocketTransformer.upgrade(request).then(handleWebSocket);
      }
      else {
        print("Regular ${request.method} request for: ${request.uri.path}");
        serveRequest(request);
      }
    });
  });
}

void serveRequest(HttpRequest request){
  request.response.statusCode = HttpStatus.forbidden;
  request.response.reasonPhrase = "WebSocket connections only";
  request.response.close();
}

void handleWebSocket(WebSocket socket){
  usersFromSocket[socket] = '';
  print('Client connected!');
  socket.listen((data) {
    Map<String, dynamic> msg = jsonDecode(data);
    print('Client sent: ${msg["action"]}');
    switch (msg["action"]){
      case 'MESSAGE' : handleMessage(data, socket); //A MESSAGE HAS BEEN SENT
      break;
      case 'USERS' : handleUsers(data, socket); //THE USER IS REQUIRING THE USERS LIST
      break;
      case 'USERNAME' : handleMyName(data, socket); //THE USER IS LOGGIN ON AND COMMUINICATING HIS NAME
      break;
      case 'ROOMMESSAGE' : handleRoomMessage(data, socket);
      break;
      default: print(data);
    }
  },
  onDone: () {
    if(usersFromSocket.keys.contains(socket)){
      usersFromName.remove(usersFromSocket[socket]);
      usersFromSocket.remove(socket);
      handleUsers([], socket);
    }
    
    print('Client disconnected');  
  });
}

void handleMyName(data, WebSocket socket){
  print(json.decode(data));
  dynamic tmp = json.decode(data);
  if(usersFromName.keys.contains(tmp["text"])){
    //socket.addError("An user with the same name already exist");
    //socket.close();
    socket.add(json.encode({
      "action":'ERROR'
    }));
    usersFromSocket.remove(socket);
    print("ERROR DOUBLE USER");
    return;
  }
  else{
    usersFromSocket[socket] = tmp["text"];
    usersFromName[tmp["text"]] = socket;
    for( WebSocket s in usersFromSocket.keys) 
      handleUsers(data, s);
  }  
}

void handleUsers(data, WebSocket socket){
  print(usersFromName.values.toList());
  socket.add(
      json.encode({
        "action":"USERLIST",
        "roomList": rooms,
        "list":usersFromSocket.values.toList()
        }
      )
  );
}

void handleMessage(data, WebSocket socket){
  dynamic tmp = json.decode(data);
  print('sending\n $tmp \nto ${tmp["to"]}');
  usersFromName[tmp["to"]].add(data); 
}

void handleRoomMessage(data, WebSocket socket){
  dynamic tmp = json.decode(data);
  print('sending\n $tmp \nto ${tmp["to"]}');
  for(WebSocket s in usersFromSocket.keys){
    print("sending a message:\n ${data}");
    if(s!=socket) 
    s.add(data); 
  }
  
}
