import 'package:flutter/material.dart';
import 'package:mychat/ChatPage.dart';
import 'main.dart';
import 'dart:convert';
import 'Globals.dart' as globals; 
import 'ChatRoomPage.dart';

class FirstPage extends StatefulWidget {
  FirstPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _FirstPageState createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {

  String stringa = "You are now connected";

  TextEditingController _textController = TextEditingController();
  ScrollController _scrollController = ScrollController();
  final GlobalKey<ScaffoldState> _scaffloidState = GlobalKey<ScaffoldState>();  

  @override
  void initState() {

    globals.streamController.stream.listen(
      (data){
        print(data);
        setState((){});
    });
    globals.channel.stream.listen((data){
      print(data.toString());
      dynamic tmp = json.decode(data);
      switch (tmp["action"]){
        case "USERLIST": {
          //print("${tmp["roomList"]} IS THE LIST");
          setState(() {
            globals.users = tmp["list"];
            globals.rooms = tmp["roomList"];
        });
          }
        break;
        case "MESSAGE":{
          if (globals.messages[ tmp["from"] ] == null)
            globals.messages[ tmp["from"] ] = List<dynamic>();
          globals.messages[ tmp["from"] ].add(tmp);
        }
        break; 
        case "ROOMMESSAGE":{
          print("I AM RECEVING A FKING MESSAGE");
          if (globals.roomMessages[ tmp["to"] ] == null)
            globals.roomMessages[ tmp["to"] ] = List<dynamic>();
          globals.roomMessages[ tmp["to"] ].add(tmp);
        } 
        break;
        case "ERROR": {
          print('error');
          _showSnackBar('An user with that name already exist');
          _restart();
          throw UnsupportedError;
          }
        break;
        default:{}
      }
      globals.streamController.sink.add("setState");

      
    },
      onDone: () => _restart(),
      onError: (error) =>_restart(error)
    );
    super.initState();
  }

  void _restart([dynamic error]){
    Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => MyHomePage(title: "Log On",)),
                  ); 
  }

  void _showSnackBar(String value) {
    if (value.isEmpty) return;
    _scaffloidState.currentState.showSnackBar(SnackBar(
      duration: Duration(seconds: 2),
      content: Text(value),
    ));
  }

  void _sendMessage() {
    //TO DO
  }

  void _openChat(String s){
    Navigator.push(context, MaterialPageRoute(builder: (context) => ChatPage(title: '$s',)));
  }

  void _openChatroom(String s){
    Navigator.push(context, MaterialPageRoute(builder: (context) => ChatroomPage(title: '$s',)));
  }

  @override
  Widget build(BuildContext context) {
    print(globals.rooms);
    return Scaffold(
      key: _scaffloidState,
      appBar: AppBar(
        title: Text(widget.title),
        
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Text('Rooms'),
              ),
            Expanded(
              child:
                ListView.builder(
                    controller: _scrollController,
                    reverse: false,
                    scrollDirection: Axis.vertical,
                    itemCount: globals.rooms.length,
                    itemBuilder: (context, index) {
                      return
                      Card(
                        child:
                        ListTile(
                          leading: Icon(Icons.supervised_user_circle, color: Colors.deepPurple,),
                          onTap: ()=>_openChatroom(globals.rooms[index]),
                          title: Text(globals.rooms[index]),
                        ) 
                      );
                    }), 
              ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Text('Clients'),
              ),
            Expanded(
              child: 
              ListView.builder(
                    controller: _scrollController,
                    reverse: false,
                    scrollDirection: Axis.vertical,
                    itemCount: globals.users.length,
                    itemBuilder: (context, index) {
                      return
                      Card(
                        child:
                        ListTile(
                          leading: Icon(Icons.supervised_user_circle, color: Colors.deepPurple,),
                          onTap: ()=>_openChat(globals.users[index]),
                          title: Text(globals.users[index]),
                        ) 
                      );
                    }) ,
            )
            
                   
          ],
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
