import 'dart:async';
import 'dart:io';
import 'package:mychat/FirstPage.dart';
import 'package:flutter/material.dart';
import 'Globals.dart' as globals;
import 'package:web_socket_channel/io.dart';
import 'dart:convert';
import 'package:web_socket_channel/status.dart' as status;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Client echo',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: MyHomePage(title: 'Log On'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  StreamSubscription _streamSubscription;
  String stringa = "You are not connected";
  TextEditingController _userController = new TextEditingController();
  final GlobalKey<ScaffoldState> _scaffloidState = GlobalKey<ScaffoldState>();

  void _showAlertDialog() {
    TextEditingController _ipv4Controller = TextEditingController();
    TextEditingController _portController = TextEditingController();

    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Modify your server options"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Flexible(
                child: TextField(
                  controller: _ipv4Controller,
                  decoration:
                      InputDecoration(hintText: 'IPv4: ${globals.ipv4}'),
                ),
              ),
              Flexible(
                child: TextField(
                  controller: _portController,
                  decoration:
                      InputDecoration(hintText: 'Port: ${globals.port}'),
                ),
              )
            ],
          ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: new Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: new Text("Save"),
              onPressed: () {
                globals.ipv4 = _ipv4Controller.text;
                globals.port = int.parse(_portController.text);
                Navigator.of(context).pop();
                // if (globals.socket != null) globals.socket.destroy();
                globals.init = false;
                setState(() {});
                //_inizialize();
                _ipv4Controller.clear();
                _portController.clear();
              },
            ),
          ],
        );
      },
    );
  }

  void _inizialize() async {
    String us = _userController.text;
    //globals.socket = IOWebSocketChannel();
    if (us == null || us.trim() == '') {
      _showSnackBar('Insert an username');
      return;
    } else {
      try {
        globals.channel =
            IOWebSocketChannel.connect("ws://${globals.ipv4}:${globals.port}");
        print("connected");
        globals.username = us;
        globals.channel.sink
            .add(jsonEncode({"action": "USERNAME", "text": "${globals.username}"}));
            //TO DO (aggiungere controllo lato server per doppio username)
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => FirstPage(
                    title: "welecome $us",
                  )),
        );
      } catch (error) {
        print(error);
        _showSnackBar('Connection failed');
      }
    }
  }

  void _showSnackBar(String value) {
    if (value.isEmpty) return;
    _scaffloidState.currentState.showSnackBar(SnackBar(
      duration: Duration(seconds: 2),
      content: Text(value),
    ));
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _streamSubscription = globals.streamController.stream.listen((data) {
      if (data is Function) data();
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffloidState,
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            onPressed: _showAlertDialog,
            icon: Icon(Icons.perm_data_setting),
          )
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FlatButton(
              onPressed: _inizialize,
              child: Icon(
                Icons.power,
                color: Colors.purple,
              ),
            ),
            Row(
              children: <Widget>[
                Spacer(
                  flex: 1,
                ),
                Flexible(
                    flex: 5,
                    child: TextField(
                      controller: _userController,
                      decoration: InputDecoration(
                        hintText: 'Username',
                      ),
                    )),
                Spacer(flex: 1),
              ],
            )
          ],
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
