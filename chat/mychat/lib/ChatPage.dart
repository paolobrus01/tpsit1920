import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mychat/main.dart';
import 'dart:convert';
import 'Globals.dart' as globals;

class ChatPage extends StatefulWidget {
  ChatPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  //List<dynamic> chatList = [];
  String stringa = "You are now connected";

  TextEditingController _textController = TextEditingController();
  ScrollController _scrollController = ScrollController();
  final GlobalKey<ScaffoldState> _scaffloidState = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    globals.streamController.stream.listen((data) {
      setState(() {});
      _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
    });
    /*
    globals.channel.stream.listen(
      (data){
        dynamic tmp = json.decode(data);
        switch (tmp['action']){
          case 'MESSAGE' : {
            /*{
              "action": "MESSAGE",
              "from" : globals.username,
              "text": _textController.text,
              "to": widget.title,
              "date": DateTime.now().toString()
            }*/
            if (tmp['to'] == globals.username)
              chatList.add(tmp);
          } break;

        }
      }
    );*/
  }

  void _showSnackBar(String value) {
    if (value.isEmpty) return;
    _scaffloidState.currentState.showSnackBar(SnackBar(
      duration: Duration(seconds: 2),
      content: Text(value),
    ));
  }

  void _sendMessage() {
    //print(globals.messages);
    if(_textController.toString().trim() == "") return;
    setState(() {
      dynamic obj = {
        "action": "MESSAGE",
        "from": globals.username,
        "text": _textController.text,
        "to": widget.title,
        "date": DateTime.now().toString()
      };
      if (globals.messages[widget.title] == null)
        globals.messages[widget.title] = List<dynamic>();
      (globals.messages[widget.title]).add(obj);
      globals.channel.sink.add(jsonEncode(obj));
      _textController.clear();
      _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
    });
  }

  //void _openChat() {}

  Widget listTileDx(dynamic message) {
    return Row(
      children: <Widget>[
        Spacer(
          flex: 1,
        ),
        Flexible(
          flex: 4,
          child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(18))),
            child: ListTile(
              title: Column(
                crossAxisAlignment:  CrossAxisAlignment.stretch,
                children: <Widget>[
                  //Text("${message["from"]}", textAlign: TextAlign.left, style: TextStyle(fontSize: 12.0, color: Colors.blueGrey, ),),
                  Text("${message["text"]}", textAlign: TextAlign.left,),
                  Text("${DateTime.parse(message["date"]).hour}:${DateTime.parse(message["date"]).minute}", textAlign: TextAlign.right, style:TextStyle(fontSize: 8)),
                ],
              ),
              contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
            ),
            color: Colors.deepPurple[100],
            elevation: 0,
          ),
        )
      ],
    );
  }

  Widget listTileSx(dynamic message) {
    return Row(
      children: <Widget>[
        Flexible(
          flex: 4,
          child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(18))),
            child: ListTile(
              title: Column(
                crossAxisAlignment:  CrossAxisAlignment.stretch,
                children: <Widget>[
                  //Text("${message["from"]}", textAlign: TextAlign.left, style: TextStyle(fontSize: 12.0, color: Colors.blueGrey, ),),
                  Text("${message["text"]}", textAlign: TextAlign.left,),
                  Text("${DateTime.parse(message["date"]).hour}:${DateTime.parse(message["date"]).minute}", textAlign: TextAlign.right, style:TextStyle(fontSize: 8)),
                ],
              ),
              contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
            ),
            elevation: 1,
          ),
        ),
        Spacer(
          flex: 1,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Color.fromRGBO(250, 245, 225, 1),
      key: _scaffloidState,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                  controller: _scrollController,
                  reverse: false,
                  scrollDirection: Axis.vertical,
                  itemCount: globals.messages[widget.title] == null
                      ? 0
                      : globals.messages[widget.title].length,
                  itemBuilder: (context, index) {
                    List<dynamic> currentMessages =
                        globals.messages[widget.title];
                    //print(chatList[index].sender + ' - ' + globals.username);
                    if ((currentMessages.elementAt(index))["from"] ==
                        globals.username)
                      return listTileDx(currentMessages.elementAt(index));
                    else
                      return listTileSx(currentMessages.elementAt(index));
                  }),
            ),
            Row(
              children: <Widget>[
                Flexible(
                    flex: 5,
                    child: TextField(
                      controller: _textController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Write here your data",
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 5)),
                    )),
                Flexible(
                  flex: 1,
                  child: FlatButton(
                    onPressed: _sendMessage,
                    child: Icon(
                      Icons.send,
                      color: Colors.purple,
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
