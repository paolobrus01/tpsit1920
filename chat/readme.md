Applicazione
============== 
L'applicazione e' formata essenzialmente da quattro diverse pagine che comunicano attraverso delle variabili globali ( Stream compresi ) che fungono da mediatori tra diversi contesti.
* La prima pagina e' di Login, qui possiamo solo modificare l'indirizzo ip del server e la porta, scegliere un Nickname e connetterci. Nel caso di errori o nel caso sia presente qualcuno con lo stesso nome nel sever non riusciremo a connetterci.
* La seconda pagina non e' altro che una coppia di ListView che rappresentano l'insieme di stanza, o chatRooms e le chat individuali; l'applicazione infatti permette sia la chat instantanea globale che quella individuale.
* Le altre due pagine sono essenzialmente simili ma con scopi diversi. Mentre una permette la comunicazione con una stanza della chatRoom, la seconda mostra i messaggi, invece di una chat singola. Questa schermata permette quindi di leggere tutti i messaggi inviati e ricevuti dall'utente o dagli utenti in base e inviarne di nuovi.

Il client fa utilizzo di WebSocket e stabilisce una connessione con il server al quale verranno inviati dei file JSON tutti contententi il campo 'action' che descrive come il server andra' a gestire il resto del dato. Allo stesso modo quando il server invia dei dati al client, il listen, dopo aver decodificato il JSON, esegue uno switch sul campo 'action' per poter manipolare il resto dei dati a dovere.


SERVER
=========
Il server creato e' un aggiornamento di [questo](https://gitlab.com/divino.marchese/flutter/tree/master/am026_websocket) server e va sostanzialmente a gestire il dato inviato da un webSocket. Specificatamente, attraverso uno switch questo individua come manipolare i dati per inviare, precisamente le diverse azioni possono essere
* case 'MESSAGE' gestisce l'invio di un messaggio da in una chat singola
* case 'USERS' invia la lista aggiornata degli utenti se richiesta dal client 
* case 'USERNAME' e' la prima interazione col server, qui viene specificato l'username dell'utente e gestito nel caso esista gia'  
* case 'ROOMMESSAGE' gestisce l'invio di un messaggio nella chatroom

