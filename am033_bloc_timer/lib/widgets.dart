import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

import 'engine.dart';


class Actions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: _mapStateToActionButtons(
        timerBloc: BlocProvider.of<TimerBloc>(context),
      ),
    );
  }

  List<Widget> _mapStateToActionButtons({
    TimerBloc timerBloc,
  }) {
    final Stato currentState = timerBloc.state;
    if (currentState.uState == UserState.ready) {
      print(currentState.uState == UserState.ready);
      return [
        FloatingActionButton(
          child: Icon(Icons.play_arrow),
          onPressed: () =>
              timerBloc.add(Event(event: UserEvent.start, duration: currentState.seconds)),
        ),
      ];
    }
    if (currentState.uState == UserState.started) {
      print(currentState.uState == UserState.ready);
      return [
        FloatingActionButton(
          child: Icon(Icons.pause),
          onPressed: () => timerBloc.add(Event(event: UserEvent.pause, duration: currentState.seconds)),
        ),
        FloatingActionButton(
          child: Icon(Icons.replay),
          onPressed: () => timerBloc.add(Event(event: UserEvent.reset, duration: currentState.seconds)),
        ),
      ];
    }
    if (currentState.uState == UserState.paused) {
      return [
        FloatingActionButton(
          child: Icon(Icons.play_arrow),
          onPressed: () => timerBloc.add(Event(event: UserEvent.start, duration: currentState.seconds)),
        ),
        FloatingActionButton(
          child: Icon(Icons.replay),
          onPressed: () => timerBloc.add(Event(event: UserEvent.reset, duration: currentState.seconds)),
        ),
      ];
    }
    if (currentState.uState == UserState.stopped) {
      return [
        FloatingActionButton(
          child: Icon(Icons.replay),
          onPressed: () => timerBloc.add(Event(event: UserEvent.reset, duration: currentState.seconds)),
        ),
      ];
    }
    return [];
  }
}

class Timer extends StatelessWidget {

  final String title;

  Timer({this.title});

  static const TextStyle timerTextStyle = TextStyle(
    fontSize: 60,
    fontWeight: FontWeight.bold,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: Stack(
        children: [
          Background(),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(vertical: 100.0),
                child: Center(
                  child: BlocBuilder<TimerBloc, Stato>(
                    builder: (context, state) {
                      print('${state.seconds} at ${state.uState}');
                      final String minutesStr = ((state.seconds / 60) % 60)
                          .floor()
                          .toString()
                          .padLeft(2, '0');
                      final String secondsStr = (state.seconds % 60)
                          .floor()
                          .toString()
                          .padLeft(2, '0');
                          
                      return Text(
                        '$minutesStr:$secondsStr',
                        style: Timer.timerTextStyle,
                      );
                    },
                  ),
                ),
              ),
              BlocBuilder<TimerBloc, Stato>(
                condition: (previousState, currentState) =>
                  currentState.uState != previousState.uState,
                    
                builder: (context, state) => Actions(),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class Background extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WaveWidget(
      config: CustomConfig(
        gradients: [
          [
            Colors.teal[700],
            Colors.teal[800],
            Colors.teal[900]
          ],
          [
            Colors.teal[400],
            Colors.teal[500],
            Colors.teal[600]
          ],
          [
            Colors.red[100],
            Colors.teal[50],
            Colors.teal[300]
          ],
        ],
        durations: [19440, 10800, 6000],
        heightPercentages: [0.03, 0.01, 0.02],
        gradientBegin: Alignment.bottomCenter,
        gradientEnd: Alignment.topCenter,
      ),
      size: Size(double.infinity, double.infinity),
      waveAmplitude: 25,
      backgroundColor: Colors.blue[100],
    );
  }
}