# Rest Client

Questa applicazione flutter utilizza [queste](https://corona.lmao.ninja/docs/#/) api col fine di visualizzare le statistiche relative ai vari paesi riguardo il COVID-19 
----
Abbiamo 4 diverse pagine
- Homepage 
    * Qui troviamo le statistiche mondiali complete, oltre ai grafici (creati con la libreria [charts_flutter 0.9.0](https://pub.dev/packages/charts_flutter) ), i grafici sono poi personabilizzabili attraverso l'uso dei settings.
    * Sul lato sinistro abbiamo la possibilità di aprire una Drawer, un widget che crea la SideBar all'interno del quale troviamo la voce settings, oltre a una barra di ricerca che ci permette di cercare il paese desiderato e selezionarlo nella sottostante ListView.
    * La voce settings aprirà un AlertBox (creato all'interno di uno StatefulBuilder per consentire il cambio dei colori) che modifica i colori dei grafici. Nonostante non abbia una vera utilità rende l'applicazione leggermente interattiva.

- CountryPage
    * Qui troviamo le statistiche relative al singolo paese, compreso di bandiere. Il colore di alcuni sottotitoli cambia a seconda delle impostazioni selezionate in precedenza.
    * Sostanzialmente la pagina è una CustomScrollView composta da una SilverAppBar (che riducendosi nasconde la bandiera) e una SilverList che non è altro che una lista di ListTile.

- AdvancedCountryPage
    * Questa pagina visualizza i grafici del paese selezionato, visualizzato secondo i colori scelti inizialmente nei settings.
    * Alcuni paesi non hanno questo tipo di statistiche: è l'unica volta in cui la pagina di errore viene richiamata senza che sia poi necessario riavviare l'app in quanto questo può non essere causato dalla connessione.

- ErrorPage
    * Questa pagina è la pagina che compare in caso di errore, l'errore gestito indica l'inconsistenza dei dati dovuta ad una non connessione a internet.

----

- L'applicazione utilizza richieste HTTP GET per prendere le api in formato JSON, queste verranno poi decodificate e verranno utilizzati i dati a disposizione.



