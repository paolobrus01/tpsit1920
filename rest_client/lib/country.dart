class Country {
  String country, iso2, iso3, flagURL, continent;
  int id, cases, todayCases, deaths,
   todayDeaths, recovered, active, critical,
   casesPerOneMillion, deathsPerOneMillion,
   tests, testsPerOneMillion;

  Country.fromJSON(Map<String, dynamic> json){
    this.country = json['country'];
    this.deaths = json['deaths'];
    this.todayDeaths = json['todayDeaths'];
    this.iso2 = json['countryInfo']['iso2'];
    this.iso3 = json['countryInfo']['iso3'];
    this.flagURL = json['countryInfo']['flag'];
    this.id = json['countryInfo']['_id'];
    this.cases = json['cases'];
    this.todayCases = json['todayCases'];
    this.recovered = json['recovered'];
    this.active = json['active'];
    this.critical = json['critical'];
    this.casesPerOneMillion = json['casesPerOneMillion'];
    this.deathsPerOneMillion = json['deathsPerOneMillion'];
    this.tests = json['tests'];
    this.testsPerOneMillion = json['testsPerOneMillion'];
  }

}