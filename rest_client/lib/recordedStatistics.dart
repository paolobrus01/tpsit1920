import 'dart:convert';
import 'myChart.dart';
import 'package:http/http.dart';

class RecordedStatistics {
  String country;
  List<ListData> cases;
  List<ListData> deaths;
  List<ListData> recovered;

  List<ListData> _convert(dynamic m){
    List<ListData> result = [];
    m.forEach((key, value){result.add(ListData(timeAsString: key,people:value));});
    return result;  
  }

  _connect() async{
    String url = 'https://corona.lmao.ninja/v2/historical/$country?lastdays=90';
    Response response;
    response = await get(url);
    if(response.statusCode!=200){return;}
    Map object =jsonDecode(response.body);
    if(country!='all') object = object['timeline'];
    this.cases = _convert(object['cases']);
    this.deaths = _convert(object['deaths']);
    this.recovered = _convert(object['recovered']); 
  }

  RecordedStatistics({this.country}){
    _connect();
  }

  @override
  String toString() {
    // TODO: implement toString
    return '{country:$country,cases:${jsonEncode(cases)},deaths:${jsonEncode(deaths)},recovered:${jsonEncode(recovered)}}';
  }

}