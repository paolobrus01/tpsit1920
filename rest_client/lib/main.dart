import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart';
import 'package:rest_client/advancedCountryPage.dart';
import 'package:rest_client/countryList.dart';
import 'alertDialog.dart';
import 'globals.dart' as globals;
import 'errorPage.dart';
import 'countryPage.dart';
import 'mainData.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => MyHomePage(title: 'Flutter Demo Home Page'),
        '/error': (context) => ErrorPage(
              title: 'Error',
            ),
        '/country': (context) => CountryPage(
              country: globals.selectedCountry,
            ),
        '/stats': (context) => AdvancedCountryPage(
              country: globals.selectedCountry,
            )
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  dynamic _data;
  dynamic _historical;
  AnimationController _controller;

  _goToErrorPage() {
    Navigator.of(context).pushReplacementNamed('/error');
  }

  _makeRequest() async {
    Response response;
    try {
      response = await get('https://corona.lmao.ninja/v2/all');
      setState(() {
        _data = jsonDecode(response.body);
      });
      response = await get('https://corona.lmao.ninja/v2/historical/all');
      setState(() {
        _historical = jsonDecode(response.body);
      });
    } catch (error) {
      _goToErrorPage();
    }
    
  }

  _requestCountries() async {
    String url = 'https://corona.lmao.ninja/v2/countries';
    Response response;
    try {
      response = await get(url);
      List countries = jsonDecode(response.body);
      globals.countries = <List<String>>[];
      countries.forEach((element) {
        globals.countries.add(<String>[
          element['country'],
          element['countryInfo']['iso2'],
          element['countryInfo']['iso3']
        ]);
      });
      setState(() {});
    } catch (error) {
      _goToErrorPage();
    }

  }

  @override
  void initState() {
    // TODO: implement initState
    _makeRequest();
    _requestCountries();
    super.initState();
    
  }

  @override
  Widget build(BuildContext context) {
    return( _data == null )?
    Container(
      color: Colors.transparent,
      child:Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children:[
        SizedBox(
        height: 50,
        width: 50,
        child: CircularProgressIndicator()
      )
      ]
    ))
    
      :
      Scaffold(
        appBar: AppBar(
          actions:<Widget>[Icon(Icons.public)] ,
          title: Text('World wide statistics',),
        ),
        body: Center(
          child: DataView(data:_data)
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            _makeRequest();
            _requestCountries();
          },
          tooltip: 'Increment',
          child: Icon(Icons.replay),
        ),
        drawer: Drawer(
            child: ListView(padding: EdgeInsets.zero, children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
            ),
            child: Text(
              'Covid-19 Statistics',
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
              ),
            ),
          ),
          ListTile(
            trailing: Icon(Icons.settings),
            title: Text('Settings', ),
            onTap: (){
              showDialog(context: context, builder:(ctx)=>createDialog(ctx)).then((x){
                setState(() {});
              });
            },
          ),
          
          CountryList(
            list: globals.countries,
            heigth: 350,
            ctx: context,
          )
        ])),
        // This trailing comma makes auto-formatting nicer for build methods.
        resizeToAvoidBottomPadding: false);
  }
}
