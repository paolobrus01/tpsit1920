import 'package:flutter/material.dart';
import 'globals.dart' as globals; 

StatefulBuilder createDialog(BuildContext context){
    Color _deathColor = globals.colors[0];
  Color _recoveredColor = globals.colors[1];
  Color _activeColor = globals.colors[2];

  return StatefulBuilder(
    builder:(context, setState){
                  
                

  List<PopupMenuEntry<Color>> _palette= [
              const PopupMenuItem<Color>(
                value: Colors.red,
                child: Text('Red'),
              ),
              const PopupMenuItem<Color>(
                value: Colors.blue,
                child: Text('Blue'),
              ),
              const PopupMenuItem<Color>(
                value: Colors.green,
                child: Text('Green'),
              ),
              const PopupMenuItem<Color>(
                value: Colors.yellow,
                child: Text('Yellow'),
              ),
              const PopupMenuItem<Color>(
                value: Colors.purple,
                child: Text('Purple'),
              ),
              const PopupMenuItem<Color>(
                value: Colors.black,
                child: Text('Black'),
              ),
            ];

  return AlertDialog(
    title: Text('Settings'),
    content: Column(
      mainAxisSize: MainAxisSize.min,
      children:[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
        Container(
          width: 150,
          child:Text("Cases color")
        ),
        PopupMenuButton<Color>(
          offset: Offset(0, 40),
          onSelected: (Color result) { setState(() { _activeColor = result; }); },
          itemBuilder: (ctx)=>_palette,
          child: Card(
            child:Padding(
              padding: EdgeInsets.only(top:5,left: 5,bottom: 5),
              child:Row(
              children: <Widget>[
              Container(
                height: 20,
                width: 20,
                color: _activeColor,
                //padding: EdgeInsets.only(top:20,left:20,right:20,bottom:0),
              ),
              Icon(Icons.arrow_drop_down)
            ],)
            )
            
          ),
        )
      ],),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
        Container(
          width: 150,
          child:Text("Deaths color")
        ),
        PopupMenuButton<Color>(
          offset: Offset(0, 40),
          onSelected: (Color result) { setState(() { _deathColor = result; }); },
          itemBuilder: (ctx)=>_palette,
          child: Card(
            child:Padding(
              padding: EdgeInsets.only(top:5,left: 5,bottom: 5),
              child:Row(
              children: <Widget>[
              Container(
                height: 20,
                width: 20,
                color: _deathColor,
                //padding: EdgeInsets.only(top:20,left:20,right:20,bottom:0),
              ),
              Icon(Icons.arrow_drop_down)
            ],)
            )
            
          ),
        )
      ],),
      Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
        Container(
          width: 150,
          child:Text("Recovered color")
        ),
        PopupMenuButton<Color>(
          offset: Offset(0, 40),
          onSelected: (Color result) { setState(() {
             _recoveredColor = result; 
             }); },
          itemBuilder: (ctx)=>_palette,
          child: Card(
            child:Padding(
              padding: EdgeInsets.only(top:5,left: 5,bottom: 5),
              child:Row(
              children: <Widget>[
              Container(
                height: 20,
                width: 20,
                color: _recoveredColor,
                //padding: EdgeInsets.only(top:20,left:20,right:20,bottom:0),
              ),
              Icon(Icons.arrow_drop_down)
            ],)
            )
            
          ),
        )
      ],),
      
      ],
    ),
    actions: <Widget>[
      FlatButton(onPressed: ()=>Navigator.of(context).pop(), child: Text('Cancel')),
      FlatButton(onPressed: (){
        globals.colors[0] = _deathColor;
        globals.colors[1] = _recoveredColor;
        globals.colors[2] = _activeColor;
        setState((){});
        Navigator.of(context).pop();
        }, child: Text('Save'))
    ]
  );});}