import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'myChart.dart';

class MyCard extends StatefulWidget{

 /*       data:widget.data,
          history:_history.cases, 
          counter:_counter, 
          title:'Cases', 
          color:charts.ColorUtil.fromDartColor(Colors.blue)*/

  List<ListData> history;
  double counter;
  int data;
  Color color;
  String title;

  MyCard({this.title, this.counter, this.history, this.data, this.color});

  @override
  _MyCardState createState()=>_MyCardState();

}

class _MyCardState extends State<MyCard>{

    String _formatString(int n){
    String ris = '${n%1000}';
    n~/=1000;
    while(n/1000 != 0){
      ris='${n%1000},$ris';
      n~/=1000;
    }
    return ris;
    
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
          child:Padding(
            padding: EdgeInsets.all(30),
            child: Column(
              children:[
                
                ListTile(
                  title:Text(
                    '${_formatString(widget.data)}', 
                    textAlign: TextAlign.center, 
                    style: TextStyle(
                        fontSize: 30
                      ),
                    ),
                  subtitle: Text('${widget.title}', textAlign: TextAlign.center, style: TextStyle(fontSize:20),),
                ),
                Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                        child: MyChart(
                          data: widget.history
                              .sublist(90-(widget.counter * 90).round()),
                          id: 'cases',
                          color: charts.ColorUtil.fromDartColor(widget.color),
                        ),
                        height: 200,
                      )
              ]
            )
            )
        
    );
  }

}