import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:rest_client/myChart.dart';
import 'package:rest_client/recordedStatistics.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'dart:async';
import 'globals.dart' as globals;


class AdvancedCountryPage extends StatefulWidget {
  AdvancedCountryPage({Key key, this.country}) : super(key: key);

  final String country;

  @override
  _AdvancedCountryPageState createState() => _AdvancedCountryPageState();
}

class _AdvancedCountryPageState extends State<AdvancedCountryPage> {
  RecordedStatistics _country;
  double _counter = 1;
  int _timer = 0;

  _goToErrorPage() {
    Navigator.of(context).popAndPushNamed('/error');
  }

  StreamSubscription _streamSub;

  @override
  void initState() {
    _timer = 0;
    try {
      _country = RecordedStatistics(country: widget.country);
      super.initState();
    } catch (error) {
      _goToErrorPage();
    }
    _streamSub = Stream<int>.periodic(Duration(milliseconds: 100), (x) => x).listen((data){
      if(_country.cases!=null) 
        setState(() {_streamSub.pause();});
        _timer++;
      if(_timer >= 30) {

        print('Error: Data cannot be get');
        Navigator.of(context).pop();
        _goToErrorPage();
        _streamSub.pause();
        }
    });
    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(widget.country),
        ),
      body:
      _country.cases == null?
      Container(
      color: Colors.transparent,
      child:Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children:[
        Center(
          child:SizedBox(
        height: 50,
        width: 50,
        child: CircularProgressIndicator()
        )
        
        
      )
      ]
    )):
      Center(
        child: Column(
          children: <Widget>[

            Container(
              padding: EdgeInsets.symmetric(vertical:20),
              child: Text('Last ${(_counter*90).round()} days', style: TextStyle(fontSize:20),),
            ),
            Slider(
              min: 1/30,
              max: 1, 
              divisions: 90, 
              label: ((_counter*90).round().toString()),
              value: _counter, 
              onChanged: (x){
                setState(() {
                  _counter = x;
              });
            },
            )
            ,
            Expanded(
              //height: 400,
              child:ListView(
              children: <Widget>[
                //CASES
                Container(
                  padding: EdgeInsets.symmetric(vertical:20),
                  child: Center(
                    child:Text('Cases', style: TextStyle(fontSize:20, ),)
                    ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical:10, horizontal:20),
                  child:MyChart(data: _country.cases.sublist(90-(_counter*90).round()), id:'cases', color: charts.ColorUtil.fromDartColor(globals.colors[2]),),
                  height: 200,
                ),
                //DEATHS
                Container(
                  padding: EdgeInsets.symmetric(vertical:20),
                  child: Center(
                    child:Text('Deaths', style: TextStyle(fontSize:20, ),)
                    ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical:10, horizontal:20),
                  child:MyChart(data: _country.deaths.sublist(90-(_counter*90).round()), id:'cases', color: charts.ColorUtil.fromDartColor(globals.colors[0]),),
                  height: 200,
                ),

                //RECOVERED
                Container(
                  padding: EdgeInsets.symmetric(vertical:20),
                  child: Center(
                    child:Text('Recovered', style: TextStyle(fontSize:20, ),)
                    ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical:10, horizontal:20),
                  child:MyChart(data: _country.recovered.sublist(90-(_counter*90).round()), id:'cases', color: charts.ColorUtil.fromDartColor(globals.colors[1]),),
                  height: 200,
                )
              ],
            )

            )
            
            
          ],
        )
        
    )
    );
  }
}