import 'package:flutter/widgets.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class MyChart extends StatelessWidget {
  final List<ListData> data;
  final String id;
  final charts.Color color;

  MyChart({@required this.data, this.id, this.color});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return charts.TimeSeriesChart(<charts.Series<ListData, DateTime>>[
      charts.Series(
          id: "${this.id}",
          domainFn: (ListData s, _) => s.time,
          measureFn: (ListData s, _) => s.people,
          data: data,
          colorFn: (_, __) => this.color)
    ]);
  }
}

class ListData {
  DateTime time;
  final String timeAsString;
  final int people;
  ListData({this.timeAsString, this.people}) {
    dynamic tmp = timeAsString.split('/');
    time = DateTime(
        int.parse('20${tmp[2]}'), int.parse(tmp[0]), int.parse(tmp[1]));
  }
}
