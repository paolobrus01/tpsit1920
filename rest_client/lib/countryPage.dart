import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:rest_client/countryList.dart';
import 'dart:async';
import 'country.dart';
import 'globals.dart' as globals;

class CountryPage extends StatefulWidget {
  CountryPage({Key key, this.country}) : super(key: key);

  final String country;

  @override
  _CountryPageState createState() => _CountryPageState();
}

class _CountryPageState extends State<CountryPage> {
  _goToErrorPage() {
    Navigator.of(context).pushReplacementNamed('/error');
  }

  _makeRequest() async {
    String url = 'https://corona.lmao.ninja/v2/countries/${widget.country}';
    Response response;
    try {
      response = await get(url);
      _country = Country.fromJSON(json.decode(response.body));
      setState(() {});
      _flag = Image.network(
        _country.flagURL,
        scale: 1,
      );
    } catch (error) {
      _goToErrorPage();
    }
  }

  Country _country;
  Image _flag;

  @override
  void initState() {
    // TODO: implement initState
    _makeRequest();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _country == null
        ? Container(
            color: Colors.white,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  SizedBox(
                      height: 50, width: 50, child: CircularProgressIndicator())
                ]))
        : Scaffold(
            body: Center(
                child: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                pinned: true,
                expandedHeight: 200.0,
                flexibleSpace: FlexibleSpaceBar(
                  title: Stack(children: [
                    Text(widget.country,
                        style: TextStyle(
                          foreground: Paint()
                            ..style = PaintingStyle.stroke
                            ..strokeWidth = 1
                            ..color = Colors.black,
                        )),
                    Text(
                      widget.country,
                      style: TextStyle(),
                    ),
                  ]),
                  background: FittedBox(
                    child: _flag ?? Container(),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              SliverList(
                delegate: SliverChildListDelegate((_country == null)
                    ? []
                    : [
                        ListTile(
                          title: Text('${_country.cases}'),
                          subtitle: Text(
                            'Cases',
                            style: TextStyle(color: globals.colors[2])),
                        ),
                        ListTile(
                          title: Text('${_country.todayCases}'),
                          subtitle: Text(
                            'Cases Today',
                            style: TextStyle(color: globals.colors[2])),
                        ),
                        ListTile(
                          title: Text('${_country.deaths}'),
                          subtitle: Text(
                            'Deaths',
                            style: TextStyle(color: globals.colors[0]),
                          ),
                        ),
                        ListTile(
                          title: Text('${_country.todayDeaths}'),
                          subtitle: Text(
                            'Today Deaths',
                            style: TextStyle(color: globals.colors[0]),
                          ),
                        ),
                        ListTile(
                          title: Text('${_country.recovered}'),
                          subtitle: Text(
                            'Recovered',
                            style: TextStyle(color: globals.colors[1]),
                          ),
                        ),
                        ListTile(
                          title: Text('${_country.active}'),
                          subtitle: Text('Active'),
                        ),
                        ListTile(
                          title: Text('${_country.critical}'),
                          subtitle: Text('Critical'),
                        ),
                        ListTile(
                          title: Text('${_country.tests}'),
                          subtitle: Text('Tests'),
                        ),
                        ListTile(
                          title: Text(
                              '${_country.casesPerOneMillion} (${_country.casesPerOneMillion / 1000000}%)'),
                          subtitle: Text('Cases per million'),
                        ),
                        ListTile(
                          title: Text(
                              '${_country.deathsPerOneMillion} (${_country.deathsPerOneMillion / 1000000}%)'),
                          subtitle: Text('Deaths per million'),
                        ),
                        ListTile(
                          title: Text(
                              '${_country.testsPerOneMillion} (${_country.testsPerOneMillion / 1000000}%)'),
                          subtitle: Text('Tests per million'),
                        ),
                        ListTile(
                          title: Text('Show more'),
                          trailing: Icon(Icons.insert_chart),
                          onTap: () {
                            Navigator.of(context).pushNamed('/stats');
                          },
                        ),
                      ]),
              ),
            ],
          )));
  }
}
