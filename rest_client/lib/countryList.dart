import 'package:flutter/material.dart';
import 'globals.dart' as globals;

class CountryList extends StatefulWidget {
  List<List<String>> list;
  double heigth;
  BuildContext ctx;

  CountryList({Key key, this.list, this.heigth, this.ctx}) : super(key: key);

  @override
  _CountryListState createState() => _CountryListState();
}

class _CountryListState extends State<CountryList> {
  String _filter = '';
  List<List<String>> _filtredList;

  @override
  void initState() {
    // TODO: implement initState
    _filtredList = widget.list;
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: TextField(
          decoration: InputDecoration(
            labelText: 'search',
            prefixIcon: Icon(Icons.search)
          ),
          onChanged: (data) {
            _filter = data.toUpperCase();
            _filtredList = [];
            _filtredList = widget.list.where((test) {
              bool b = (test[0] ?? '').toUpperCase().startsWith(_filter) ||
                  (test[1] ?? '').toUpperCase().startsWith(_filter) ||
                  (test[2] ?? '').toUpperCase().startsWith(_filter);
              return b;
            }).toList();

            setState(() {});
          },
        ),
      ),
      Container(
        height: widget.heigth,
        child: ListView.builder(
          itemCount: _filtredList.length,
          itemBuilder: (context, index) {
            return ListTile(
              onTap: () {
                globals.selectedCountry = _filtredList[index][0];
                Navigator.of(widget.ctx).pushNamed('/country');
              },
              title: Text(
                '${_filtredList[index][0]}',
                style: TextStyle(),
                textAlign: TextAlign.center,
              ),
            );
          },
        ),
      ),
    ]);
  }
}
