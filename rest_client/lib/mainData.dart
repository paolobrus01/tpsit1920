import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:rest_client/recordedStatistics.dart';
import 'globals.dart' as globals;
import 'dataCard.dart';
import 'myChart.dart';

class DataView extends StatefulWidget {
  Map data = {};

  DataView({this.data});

  @override
  _DataViewState createState() => _DataViewState();
}

class _DataViewState extends State<DataView> {
  RecordedStatistics _history = RecordedStatistics(country: 'all');
  int _timer = 0;
  double _counter = 1;
  StreamSubscription _streamSub;

  @override
  void initState() {
    // TODO: implement initState
    _streamSub = Stream<int>.periodic(Duration(milliseconds: 100), (x) => x)
        .listen((data) {
      if (_history.cases != null)
        setState(() {
          _streamSub.pause();
        });
      _timer++;
      if (_timer >= 30) {
        print('Error: Data cannot be get');
        Navigator.of(context).pushReplacementNamed('/error');
        _streamSub.pause();
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _history.cases == null
        ? Container(
            color: Colors.white,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  SizedBox(
                      height: 50, width: 50, child: CircularProgressIndicator())
                ]))
        : ListView(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(vertical: 20),
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical:20),
                  child:Column(
                  children: <Widget>[
                    Slider(
                      min: 1 / 30,
                      max: 1,
                      divisions: 90,
                      label: ((_counter * 90).round().toString()),
                      value: _counter,
                      onChanged: (x) {
                        setState(() {
                          _counter = x;
                        });
                      },
                    ),
                    Text(
                        'Show charts for the last ${(_counter * 90).round()} days'),
                  ],
                ),
                )
              ),
              MyCard(
                  data: widget.data['cases'],
                  history: _history.cases,
                  counter: _counter,
                  title: 'Cases',
                  color: globals.colors[2]),
              MyCard(
                  data: widget.data['deaths'],
                  history: _history.deaths,
                  counter: _counter,
                  title: 'Deaths',
                  color: globals.colors[0]),
              MyCard(
                  data: widget.data['recovered'],
                  history: _history.recovered,
                  counter: _counter,
                  title: 'Recovered',
                  color: globals.colors[1]),
                 
              ListTile(
                title: Text('${widget.data['critical']}'),
                subtitle: Text('Critical', style: TextStyle(color:globals.colors[0]),),
              ), 
              ListTile(
                title: Text('${widget.data['recovered']}'),
                subtitle: Text('Recovered', style:TextStyle(color:globals.colors[1])),
              ),
              ListTile(
                title: Text('${widget.data['active']}'),
                subtitle: Text('Active'),
              ), 
              ListTile(
                title: Text('${widget.data['todayCases']}'),
                subtitle: Text('Today Cases'),
              ),
              ListTile(
                title: Text('${widget.data['todayDeaths']}'),
                subtitle: Text('Today Deaths'),
              ),
              ListTile(
                title: Text('${widget.data['tests']}'),
                subtitle: Text('Tests'),
              ),
              ListTile(
                          title: Text(
                              '${widget.data['casesPerOneMillion']} (${widget.data['casesPerOneMillion'] / 1000000}%)'),
                          subtitle: Text('Cases per million'),
                        ),
                        ListTile(
                          title: Text(
                              '${widget.data['deathsPerOneMillion']} (${widget.data['deathsPerOneMillion'] / 1000000}%)'),
                          subtitle: Text('Deaths per million'),
                        ),
                        ListTile(
                          title: Text(
                              '${widget.data['testsPerOneMillion']} (${widget.data['testsPerOneMillion'] / 1000000}%)'),
                          subtitle: Text('Tests per million'),
                        ),
            ],
          );
  }
}
