import 'package:flutter/material.dart';

class ErrorPage extends StatefulWidget {
  ErrorPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ErrorPageState createState() => _ErrorPageState();
}

class _ErrorPageState extends State<ErrorPage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          leading: Icon(Icons.error),
        ),
        body: Center(
          
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Padding(
                padding: EdgeInsets.all(10),
                child: Text('Error:', style: TextStyle(fontSize:30),),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child:  Column(
                  children: <Widget>[
                    Text("We couldn't get your data"),
                    Text('they may not exist or are not reachable.'),
                    Text(''),
                    Text('Check your internet'),
                    Text('connection and try again')
                  ],
                )
              ),
              Icon(Icons.signal_cellular_connected_no_internet_4_bar, size:30)
            ]
          )

          ) 
        
        );
  }
}
