Server Echo
==============
### Server
Il server non e' altro che un SeverSocket che viene creato in ascolto di qualunque client riesca a connettersi, ogni volta che un client invia dei dati il server invia al client lo stesso messaggio ma in maiuscolo.

### Client
Il client e' creato in modo da poter facilmente essere riciclato per il progetto chatRoom, ci sono 3 file principali
* main.dart in cui e' presente la prima pagina, di login in cui viene inizializzato il socket tramite il relativo pulsante e, volendo si possono modificare le impostazioni del server host (Ipv4 e Porta)
* firstPage.dart in cui e' presente essenzialmente la seconda pagina impostata come una chat in cui possono essere inviati dei messaggi e si riceve (quasi immediatamente a causa della velocita' della rete locale) la risposta del server.
* globals in cui sono salvate le variabili globali, come ad esempio il socket.

Non ho usato altro che le mie conoscenze e la documentazione flutter.