import 'package:flutter/material.dart';
import 'dart:convert';
import 'Globals.dart' as globals; 

class FirstPage extends StatefulWidget {
  FirstPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _FirstPageState createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {

  String stringa = "You are now connected";

  TextEditingController _textController = TextEditingController();
  ScrollController _scrollController = ScrollController();
  final GlobalKey<ScaffoldState> _scaffloidState = GlobalKey<ScaffoldState>();  

  void _showSnackBar(String value) {
    if (value.isEmpty) return;
    _scaffloidState.currentState.showSnackBar(SnackBar(
      duration: Duration(seconds: 2),
      content: Text(value),
    ));
  }

  void _sendMessage() {
    //print("$init mess") ;
    try {
      setState(() {
        globals.list.add(
        globals.Mess(
          text:_textController.text,
          reciever: 'THIS'
          )
      );
      });
      
      globals.socket.add(utf8.encode(_textController.text));
      _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
    } catch (error) {
      globals.init = false;
      print("ERROR HERE");
      print(error);
      globals.socket.destroy();
      globals.socket = null;
      setState(() {});
      _showSnackBar("You have been disconnected");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffloidState,
      appBar: AppBar(
        title: Text(widget.title),
        
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
              Container(
                child: Text(stringa, style: TextStyle(color: Colors.green)),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              ),
              Flexible(
                child: ListView.builder(
                    controller: _scrollController,
                    reverse: false,
                    scrollDirection: Axis.vertical,
                    itemCount: globals.list.length,
                    itemBuilder: (context, index) {
                      if(globals.list[index].reciever == 'THIS') return 
                      Row(
                        children: <Widget>[
                          Spacer(flex: 1,),
                          Flexible(
                            flex: 4,
                            child: Card(
                              shape:  RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(18))),
                              child: ListTile(
                                title: Text(
                                  "${globals.list.elementAt(index).text}"),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                                  ),
                                color: Color.fromRGBO(252,228,236, 0.9),
                           ),
                          ) 
                        ],
                      );
                      else return
                      Row(
                        children: <Widget>[
                          Flexible(
                            flex: 4,
                            child: Card(
                              shape:  RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(18))),
                              child: ListTile(
                                title: Text(
                                  "${globals.list.elementAt(index).text}"),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                                  ),
                           ),
                          ),
                          Spacer(flex: 1,),
                        ],
                      );
                    }),
              ),
              Row(
                children: <Widget>[
                  Flexible(
                    flex: 5,
                    child: TextField(
                      controller: _textController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Write here your data",
                          contentPadding:
                              EdgeInsets.symmetric(horizontal: 20, vertical: 10)),
                    )
                  ),
                  Flexible(
                    flex: 1,
                    child: FlatButton(
                      onPressed: _sendMessage,
                      child: Icon(
                        Icons.send,
                        color: Colors.red,
                      ),
                    ),
                  )
                ],
              )
          ],
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
