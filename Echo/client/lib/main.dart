import 'dart:io';
import 'package:client/FirstPage.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'Globals.dart' as globals;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Client echo',
      theme: ThemeData(
        primarySwatch: Colors.pink,
      ),
      home: MyHomePage(title: 'myClient'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String stringa = "You are not connected";

  ScrollController _scrollController = ScrollController();
  final GlobalKey<ScaffoldState> _scaffloidState = GlobalKey<ScaffoldState>();

  void _showAlertDialog() {
    
  TextEditingController _ipv4Controller = TextEditingController();
  TextEditingController _portController = TextEditingController();
  
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Modify your server options"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Flexible(
                child: TextField(
                  controller: _ipv4Controller,
                  decoration: InputDecoration(hintText: 'IPv4: ${globals.ipv4}'),
                ),
              ),
              Flexible(
                child: TextField(
                  controller: _portController,
                  decoration: InputDecoration(hintText: 'Port: ${globals.port}'),
                ),
              )
            ],
          ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: new Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: new Text("Save"),
              onPressed: () {
                globals.ipv4 = _ipv4Controller.text;
                globals.port = int.parse(_portController.text);
                Navigator.of(context).pop();
                if (globals.socket != null) globals.socket.destroy();
                globals.init = false;
                setState(() {});
                //_inizialize();
                _ipv4Controller.clear();
                _portController.clear();
              },
            ),
          ],
        );
      },
    );
  }

  void _inizialize() async {
    print("${globals.ipv4} & ${globals.port}");
    setState(() {
      stringa = "trying to connect";
    });

    globals.socket = await Socket.connect(globals.ipv4, globals.port).catchError((onError) {
      setState(() {
        globals.init = false;
      });
      print(onError.toString());
      _showSnackBar("Failed to connect");
    });

    if (globals.socket != null) {
      _showSnackBar("Connected");
      setState(() {
        globals.init = true;
        stringa = "Connected";
      });

      globals.socket.listen((List<int> event) {
        
        setState(() {
          //stringa = utf8.decode(event);
          globals.list.add(
          globals.Mess(
          text: utf8.decode(event),
          reciever: 'SERVER'
        ));
      _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
        });
      }, onError: (error) {
        globals.init = false;
        globals.socket.destroy();
        globals.socket = null;
        setState(() {});
        _showSnackBar("You have been disconnected");
      }, onDone: () {
        globals.init = false;
        globals.socket.destroy();
        globals.socket = null;
        setState(() {});
        _showSnackBar("You have been disconnected");
        globals.list.clear();
      });
    
      Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => FirstPage(title: (globals.userController.text==null||globals.userController.text=="") ?"Welecome User":"Welecome ${globals.userController.text}",)),
            );

    }
  }

  void _showSnackBar(String value) {
    if (value.isEmpty) return;
    _scaffloidState.currentState.showSnackBar(SnackBar(
      duration: Duration(seconds: 2),
      content: Text(value),
    ));
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffloidState,
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            onPressed: _showAlertDialog,
            icon: Icon(Icons.perm_data_setting),
          )
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FlatButton(
                    onPressed: _inizialize,
                    child: Icon(
                      Icons.power,
                      color: Colors.red,
                    ),
            ),
            Row(
              children: <Widget>[
                Spacer(flex: 1,),
                Flexible(
                  flex: 5,
                  child: TextField(
                  controller: globals.userController,
                  decoration: InputDecoration(hintText: 'Username', ),
                
                )
                ),
                Spacer(flex: 1),
              ],
            )
            
            
          ],
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}


