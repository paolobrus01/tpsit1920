import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/rendering.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        brightness: Brightness.light,
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
      ),
      home: MyHomePage(title: 'Cronometro'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  IconData _playStopIcon = Icons.play_arrow;
  int _secondi = 0;
  int _difference = 0;
  final Duration _time = Duration(milliseconds: 100);

  StreamController<int> _streamController = StreamController.broadcast();
  StreamSubscription _cronometro;
  Stream<int> stream;

  List<int> _items = [];
  List<int> _itemsFUll = [];

  bool _isRunning = false;

  String _toTime(int time) {
    return (time ~/ 36000).toString().padLeft(2, '0') +
        ':' + //HOURS
        ((time ~/ 600) % 60).toString().padLeft(2, '0') +
        ':' + //MINUTES
        ((time ~/ 10) % 60).toString().padLeft(2, '0') +
        ',' + //SECONDS
        (time % 10).toString(); // 1/10 of SECOND
  }

  //INIZIALIZZAZIONE STREAMCONTROLLER E SUBSCRIBE ALLO STREAM PERIODICO
  void _play() {
    if (_cronometro == null)
      _streamController.addStream(Stream<int>.periodic(_time));
    _cronometro = _streamController.stream.listen((data) {
      _secondi++;
      _difference++;
      setState(() {
      });
    });
    setState(() {
      _playStopIcon = Icons.stop;
      _isRunning = true;
    });
  }

  //STOPPA MOMENTANEAMENTE IL CRONOMETRO CANCELLANDO LA SOTTOSCRIZIONE ALLO STREAM DELL'OGGETTO CRONOMETRO
  void _stop() {
    _cronometro.cancel();
    setState(() {
      _playStopIcon = Icons.play_arrow;
      _isRunning = false;
    });
  }

  //STOPPA E AZZERA IL CRONOMETRO INSIEME AGLI EVENTUALI RECORDS
  void _restart() {
    if (_isRunning) _stop();
    _items = [];
    _itemsFUll = [];
    setState(() {
      _difference = 0;
      _secondi = 0;
    });
  }

  //AGGIUNTA DI UN RECORD ALLA LISTA E AGGIORNAMENTO UI
  void _timer(){
    _items.add(_difference);
    _itemsFUll.add(_secondi);
    setState(() {
      _difference = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Spacer(
              flex: 3,
            ),
            Text(
              _toTime(_secondi),
              style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: 25,
              ),
            ),
            SizedBox(height: 25), //SEPARATORE VUOTO TRA IL TEMPO E I BOTTONI
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FloatingActionButton(
                  onPressed: _isRunning ? _stop : _play,
                  tooltip: 'Play/Stop',
                  child: Icon(_playStopIcon),
                ),
                SizedBox(width: 30), //SPAZIO TRA I PULSANTI
                FloatingActionButton(
                  onPressed: _restart,
                  tooltip: 'Restart',
                  child: Icon(Icons.refresh),
                ),
                SizedBox(width: 30), //SPAZIO TRA I PULSANTI
                FloatingActionButton(
                  onPressed: _timer,
                  tooltip: 'Record',
                  child: Icon(Icons.access_alarm),
                ),
              ],
            ),
            SizedBox(height: 10), //SPAZIO VUOTO TRA LA LISTA E I PULSANTI
            Flexible(
              flex: 5,
              child: ListView.builder(
                  reverse : false,
                  scrollDirection: Axis.vertical,
                  itemCount: _items.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(
                        _toTime(_items[index])+ ' at: ' +_toTime(_itemsFUll[index]),
                        textAlign: TextAlign.center  
                        )
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
