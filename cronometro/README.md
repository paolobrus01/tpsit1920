# Cronometro

Applicazione realizzata con flutter, testata su dispositivi android, consiste in un semplice cronometro con le funzioni base di play/pausa, restart e una funzionalità aggiuntiva per la segnalazione di tempi intermedi.

## L'interfaccia grafica

L'intefaccia è realizzata in modo che possa essere adattata facilmente a ogni schermo attraverso l'uso dei Widget [Flexible](https://api.flutter.dev/flutter/widgets/Flexible-class.html), [Spacer](https://api.flutter.dev/flutter/widgets/Spacer-class.html) e [Expanded](https://api.flutter.dev/flutter/widgets/Expanded-class.html).
L'utente ha a disposizione 3 pulsanti
- Start/Stop che aziona o mette in pausa il timer del cronometro.
- Reload che azzera il cronometro e elimina i tempi intermedi.
- Intermediate Time che calcola i tempi intermedi, può essere utilizzato, ad esempio per calcolare quanto tempo un corridore impiega a fare 3 giri senza dover fermare il cronometro alla fine di ogni giro.

## Il codice

Il codice fa uso di uno StreamController all'interno del quale viene "aggiunto" uno stream periodico con cadenza di 0,1 secondi. Lo stream, quando il cronometro è attivo, viene ascoltato dall'oggetto _cronometro (privato) che aggiorna la variabile _secondi (che in realtà rappresenta i decimi di secondi) e l'interfaccia grafica così che si veda lo scorrere del tempo.

Documentazioni:

- [Documentazione Flutter](https://flutter.dev/docs)
- [Documentazione Dart](https://dart.dev/guides/language/language-tour)



