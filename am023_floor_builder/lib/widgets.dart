import 'package:flutter/material.dart';

import 'task.dart';
import 'task_dao.dart';

class ListCell extends StatelessWidget {
  ListCell({
    Key key,
    @required this.task,
    @required this.dao,
  }) : super(key: key);

  Task task;
  final TaskDao dao;

  _showAlertDialog(context){
    TextEditingController _myText = TextEditingController();

    showDialog(context: context,
    builder:(BuildContext context){
      _myText.text = task.message;
      return AlertDialog(
        title: Text('Edit your Task'),
        content: TextField(
          controller: _myText,
          decoration: InputDecoration(
            hintText: 'Edit'
            ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Cancel'), 
            onPressed: (){
              Navigator.of(context).pop();
          },
          ),
          FlatButton(
            child: Text('Save'), 
            onPressed: ()async{
              task.message = _myText.text;
              await dao.updateTask(task);
              Navigator.of(context).pop();
            },
          )
        ],
      );
    });

  }

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: Key('${task.hashCode}'),
      confirmDismiss: (direction) async {
        if (direction == DismissDirection.startToEnd) {
          //EDIT ITEM
          _showAlertDialog(context);
          return false;
        } else {
          return true;
        }
      },
      background: Container(
        color: Colors.blue,
        alignment: Alignment.centerLeft,
        child: Icon(Icons.edit, color: Colors.white),
      ),
      secondaryBackground: Container(
        color: Colors.red,
        alignment: Alignment.centerRight,
        child: Icon(Icons.delete, color: Colors.white),
      ),
      //direction: DismissDirection.endToStart,
      child: ListTile(title: Text(task.message)),
      onDismissed: (direction) async {
        await dao.deleteTask(task);
        Scaffold.of(context).showSnackBar(
          SnackBar(
            content: const Text('Removed task'),
            duration: Duration(milliseconds: 250),
          ),
        );
      },
    );
  }
}
